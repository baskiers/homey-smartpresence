# Smart Presence

Detects the presence of humans by their smartphones. The app works on detecting closed TCP ports of smart phones on your Wifi networks. This means it functions without installing the Homey app, allowing you to detect guests as well.

## Buy me a beer

If you like this app

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="VENTP7VXTLRNW" />
<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate" />
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>

## Adding device

For adding a device, please follow the following steps:

1. Give guest your wifi password.
2. Check the IP address off the smartphone. Most router user interfaces support this.
3. Make a DHCP reservation for the IP address, this makes sure that the device will get the same address every time. Again: most routers support DHCP reservations in their user interface.
4. Enter the IP address of the smartphone
5. Check 'Is Guest' if the smartphone does belong to a house guest. Don't check it if the smartphone belongs to a household member.

## Triggers

* Someone came home
* Someone left home
* First person came home
* Last person left home 
* A guest arrived
* A guest left
* The first guest arrived
* The last guest left
* A household member came home
* A household member left home
* The first household member came home
* The last household member left home

## Conditions

* Someone is home
* Someone left home
* Having guests
* Not having guests
* Household members home
* No household members home

## Upcoming features
* Trigger: Nobody has been home for 'x' time
* Condition: Nobody has been home for 'x' time
* Person specific triggers and conditions
* Feature requests can be posted on [BitBucket.org](https://bitbucket.org/terryhendrix/homey-smartpresence/issues?status=new&status=open)

## Release history

### 0.2.2
* Improved dutch translations
* Remove host interval and host timeout settings. It will always run with interval 1 and timeout 3.
* Fix 'log is undefined' exception.
* Changed "Offline" to "Away" when a device is away.

### 0.2.1
* Household member triggers & conditions.
* Add 'Away delay'. This is required because some smartphones disconnect from the Wifi. They reconnect once in a while. This setting allows you to specify how it should take before the smartphone gets the 'Away' status.
* Monkey proofing.

### 0.2.0
* Guests triggers & conditions.
* Adding 'is guest' to device config.

### 0.1.0
* Initial presence implementation, using the [Net Scan app](https://apps.athom.com/app/nl.terryhendrix.netscan).
* Someone / nobody triggers & conditions


## Related projects
If you're looking for a TCP port monitoring app, checkout my [Net Scan app](https://apps.athom.com/app/nl.terryhendrix.netscan). 


"use strict";

module.exports.init = function() {
	Homey.log("Init IP Scan");
};


console.logWithLevel = function (message, level) {

	if (!level)
		level = "debug";

	level = level.toUpperCase();

	// Save log message to settings
	// Retreive current logs
	let currentLogs = Homey.manager('settings').get('currentLogs');

	if (!currentLogs)
		currentLogs = [];

	var record = {
		datetime: new Date(new Date().getTime()),
		message: message,
		level: level
	};

	currentLogs.push(record);

	var logCount = parseInt(Homey.manager('settings').get('log_message_count'));
	if(!logCount)
		logCount = 50;

	if (currentLogs.length > logCount)
		currentLogs.splice(0, currentLogs.length - logCount);

	Homey.manager('settings').set('currentLogs', currentLogs);
};

console.info = function () {
	let logArguments = Array.from(arguments);
	logArguments.forEach(function (part, index, theArray) {
		theArray[index] = JSON.stringify(part)
	});
	var msg = logArguments.join(' ');
	console.logWithLevel(msg, "INFO");
	this.apply(console, arguments)
}.bind(console.info);

console.warn = function () {
	let logArguments = Array.from(arguments);
	logArguments.forEach(function (part, index, theArray) {
		theArray[index] = JSON.stringify(part)
	});
	var msg = logArguments.join(' ');
	console.logWithLevel(msg, "ERROR");
	this.apply(console, arguments)
}.bind(console.warn);

console.error = function () {
	let logArguments = Array.from(arguments);
	logArguments.forEach(function (part, index, theArray) {
		theArray[index] = JSON.stringify(part)
	});
	var msg = logArguments.join(' ');
	console.logWithLevel(msg, "ERROR");
	this.apply(console, arguments)
}.bind(console.error);

console.log = function () {
	let logArguments = Array.from(arguments);
	logArguments.forEach(function (part, index, theArray) {
		theArray[index] = JSON.stringify(part)
	});
	var msg = logArguments.join(' ');
	console.logWithLevel(msg, "DEBUG");
	this.apply(console, arguments)
}.bind(console.log);

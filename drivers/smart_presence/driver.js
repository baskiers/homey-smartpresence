"use strict";

// a list of devices, with their 'id' as key
// it is generally advisable to keep a list of
// paired and active devices in your driver's memory.
var devices = {};
var triggers = {};
const HOST_CHECK_INTERVAL = 500;
const HOST_TIMEOUT = 3000;



// The ICMP driver works by connecting to a port and checking which error response one gets.
// We have to assume a port is closed, this assumption is corrected if a device appears to have the port open anyway.
const ASUMED_CLOSED_PORT = 1;

// https://www.tutorialspoint.com/nodejs/nodejs_net_module.htm
var net = require("net");
function epoch() {
    return new Date().getTime();
}

// a helper method to add a device to the devices list
function initDevice(device_data) {
    module.exports.getSettings(device_data, function (err, settings) {
        devices[device_data.id] = {};
        devices[device_data.id].state = {person_present: false};
        devices[device_data.id].data = device_data;
        devices[device_data.id].settings = settings;
        devices[device_data.id].lastSeenAt = epoch() - (settings.away_delay * 1001);    // Correct lastSeenAt with delay, so can trigger off right away
        devices[device_data.id].firstCheckPerformed = false;    // Used to make sure the first check will never trigger
        devices[device_data.id].present = false;    // Used to make sure the first check will never trigger

        var reload = function() {
            module.exports.getSettings(device_data, function (err, _settings) {
                if(devices[device_data.id] && devices[device_data.id].state &&
                    devices[device_data.id].state.person_present) {
                    devices[device_data.id].present = devices[device_data.id].state.person_present;
                }

                if(_settings)
                    devices[device_data.id].settings = _settings;

                var tokens = { "who" : devices[device_data.id].settings.name };
                var state = devices[device_data.id].state;

                var handleResult = function (err, result) {
                    if (err) return console.error(err);
                };

                var isOnlineCallback = function (d) {
                    devices[device_data.id].lastSeenAt = epoch();

                    if(!devices[device_data.id].present && devices[device_data.id].firstCheckPerformed) {
                        console.info("Device " + d.settings.host + " is home");
                        Homey.manager('flow').trigger('someone_entered', tokens, handleResult);
                        module.exports.setAvailable( device_data );

                        var someoneWasAtHome = someoneAtHome();
                        var wasHavingGuests = havingGuests();
                        var housHoldMemberWasHome = houseHoldMemberHome();

                        devices[device_data.id].present = true;

                        if(!someoneWasAtHome && someoneAtHome()) {
                            console.info("The first person entered.");
                            Homey.manager('flow').trigger('first_person_entered', tokens, handleResult);
                        }

                        if(devices[device_data.id].settings.is_guest) {
                            Homey.manager('flow').trigger('guest_arrived', tokens, handleResult);
                            if(!wasHavingGuests && havingGuests()) {
                                Homey.manager('flow').trigger('first_guest_arrived', tokens, handleResult);
                            }
                        }
                        else {
                            Homey.manager('flow').trigger('household_member_arrived', tokens, handleResult);
                            if(!housHoldMemberWasHome && houseHoldMemberHome()) {
                                Homey.manager('flow').trigger('first_household_member_arrived', tokens, handleResult);
                            }
                        }
                    }

                    devices[device_data.id].firstCheckPerformed = true;
                    triggers[device_data.id] = setTimeout(reload, HOST_CHECK_INTERVAL);
                };

                var isOfflineCallback = function (d) {
                    if(!devices[device_data.id].present && !devices[device_data.id].firstCheckPerformed) {
                        module.exports.setUnavailable(device_data, "Away");
                    }

                    if(devices[device_data.id].present && devices[device_data.id].firstCheckPerformed &&
                        epoch() - devices[device_data.id].lastSeenAt > (devices[device_data.id].settings.away_delay * 1000)) {

                        console.info("Device " + d.settings.host + " went away");
                        Homey.manager('flow').trigger('someone_left', tokens, handleResult);
                        module.exports.setUnavailable(device_data, "Away");

                        var someoneWasAtHome = someoneAtHome();
                        var wasHavingGuests = havingGuests();
                        var housHoldMemberWasHome = houseHoldMemberHome();

                        devices[device_data.id].present = false;

                        if(someoneWasAtHome && !someoneAtHome()) {
                            console.info("The last person left.");
                            Homey.manager('flow').trigger('last_person_left', tokens, handleResult);
                        }

                        if(devices[device_data.id].settings.is_guest) {
                            Homey.manager('flow').trigger('guest_left', tokens, handleResult);
                            if(wasHavingGuests && !havingGuests()) {
                                Homey.manager('flow').trigger('last_guest_left', tokens, handleResult);
                            }
                        }
                        else {
                            Homey.manager('flow').trigger('household_member_left', tokens, handleResult);
                            if(housHoldMemberWasHome && !houseHoldMemberHome()) {
                                Homey.manager('flow').trigger('last_household_member_left', tokens, handleResult);
                            }
                        }
                    } else if(devices[device_data.id].present && devices[device_data.id].firstCheckPerformed) {
                        console.info("Device " + d.settings.host + " is last seen at "+new Date(devices[device_data.id].lastSeenAt));
                    }

                    devices[device_data.id].firstCheckPerformed = true;
                    triggers[device_data.id] = setTimeout(reload, HOST_CHECK_INTERVAL);
                };

                var sanityCheck = function(d) {
                    scanDevice(devices[device_data.id], isOnlineCallback, isOfflineCallback);
                };

                scanDevice(devices[device_data.id], isOnlineCallback, sanityCheck);
            });
        };

        triggers[device_data.id] = setTimeout(reload, 0);
    });
}

// a helper method to get a device from the devices list by it's device_data object
function getDeviceByData(device_data) {
    var device = devices[device_data.id];
    if (typeof device === 'undefined') {
        return new Error("Could not find device " + device_data.id);
    } else {
        return device;
    }
}

function someoneAtHome() {
    for(var key in devices) {
        if(devices.hasOwnProperty(key) && devices[key] && devices[key].state &&
           devices[key].present) {
            return true;
        }
    }
    return false;
}


function houseHoldMemberHome() {
    for(var key in devices) {
        if(devices.hasOwnProperty(key) && devices[key] && devices[key].state &&
            devices[key].present && !devices[key].settings.is_guest) {
            return true;
        }
    }
    return false;
}

function havingGuests() {
    for(var key in devices) {
        if(devices.hasOwnProperty(key) && devices[key] && devices[key].state &&
           devices[key].present && devices[key].settings.is_guest) {
            return true;
        }
    }
    return false;
}

function scanDevice(device, onlineCallback, offlineCallback) {
    var client = new net.Socket();
    var cancelCheck = setTimeout(function() {
        client.destroy();
        handleOffline();
    }, HOST_TIMEOUT);

    var handleOnline = function () {
        device.state = {person_present: true};
        clearTimeout(cancelCheck);
        client.destroy();
        if (onlineCallback)
            onlineCallback(device);

    };

    var handleOffline = function () {
        device.state = {person_present: false};
        clearTimeout(cancelCheck);
        client.destroy();
        if (offlineCallback) {
            offlineCallback(device);
        }
    };

    client.on('error', function (err) {
        if(err && err.errno && err.errno == "ECONNREFUSED") {
            handleOnline();
        }
        else if(err && err.errno && err.errno == "EHOSTUNREACH") {
            handleOffline();
        }
        else if(err && err.errno && err.errno == "ENETUNREACH") {
            console.error("The network that the configured smartphone is on, is not reachable. Are you sure the Homey can reach the configured IP?");
            handleOffline();
        }
        else if(err && err.errno) {
            console.error("ICMP driver can only handle ECONNREFUSED, ENETUNREACH and EHOSTUNREACH, but got "+err.errno);
            handleOffline();
        }
        else {
            console.error("ICMP driver can't handle "+err);
            handleOffline();
        }
    });


    try {
        if(device && device.settings && device.settings.host) {
            client.connect(ASUMED_CLOSED_PORT, device.settings.host.trim(), function () {
                handleOnline();
            });
        }
        else {
            handleOffline();
        }
    } catch(ex) {
        console.error(ex.message);
        handleOffline();
    }
}

// the `init` method is called when your driver is loaded for the first time
module.exports.init = function (devices_data, callback) {
    console.info("Booting smartphone driver");
    devices_data.forEach(function (device_data) {
        initDevice(device_data);
    });

    callback();
};

// the `added` method is called is when pairing is done and a device has been added
module.exports.added = function (device_data, callback) {
    console.info("Adding device " + device_data.id);
    initDevice(device_data);
    callback(null, true);
};

// the `delete` method is called when a device has been deleted by a user
module.exports.deleted = function (device_data, callback) {
    console.info("Deleting device " + device_data.id);
    clearTimeout(triggers[device_data.id]);
    delete devices[device_data.id];
    callback(null, true);
};

// the `pair` method is called when a user start pairing
module.exports.pair = function (socket) {
    console.info("Pairing started");
    socket.on('configure_ip', function (data, callback) {
        console.info("Configuring device");
        callback(null, data);
    })
};

module.exports.renamed = function( device_data, new_name ) {
    module.exports.getSettings(device_data, function (err, _settings) {
        _settings.name = new_name;
        module.exports.setSettings(device_data, _settings);
    });
};

// these are the methods that respond to get/set calls from Homey
// for example when a user pressed a button
module.exports.capabilities = {};

module.exports.capabilities.person_present = {};

module.exports.capabilities.person_present.get = function (device_data, callback) {
    var device = getDeviceByData( device_data );
    if( device instanceof Error ) return callback( device );
    return callback( null, device.state.person_present );
};

Homey.manager('flow').on('condition.someone_at_home', function (callback, args) {
    callback(null, someoneAtHome());
});

Homey.manager('flow').on('condition.having_guests', function (callback, args) {
    callback(null, havingGuests());
});

Homey.manager('flow').on('condition.a_household_member_is_home', function (callback, args) {
    callback(null, houseHoldMemberHome());
});